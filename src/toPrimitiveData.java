public class toPrimitiveData {
    public static void unBoxing(){

        Byte byteobj = 11;
        Short shortobj = 21;
        Integer intobj = 31;
        Long longobj = 41L;
        Float floatobj = 51.0f;
        Double doubleobj = 61.0D;
        Character chartobj = 'c';
        Boolean boolobj = false;
        /*
         * Unboxing : converting Objects to Primitives
         * Unboxing là cơ chế tự động chuyển đổi các object của wrapper class sang kiểu dữ liệu nguyên thủy tương ứng
         */

         byte bytevalue = byteobj;
         short shortvalue = shortobj;
         int intvalue = intobj;
         long longvalue = longobj;
         float floatvalue = floatobj;
         double doublevalue = doubleobj;
         char chartvalue = chartobj;
         boolean boovalue = boolobj;

        System.out.println("--Printing primitives value (In giá tri cua các Primitives DataType (Kieu du lieu nguyên thuy)---");
        System.out.println("byte value: " + bytevalue);
        System.out.println("short value: " + shortvalue);
        System.out.println("int value: " + intvalue);
        System.out.println("long value: " + longvalue);
        System.out.println("float value: " + floatvalue);
        System.out.println("double value: " + doublevalue);
        System.out.println("char value: " + chartvalue);
        System.out.println("boolean value: " + boovalue);
    }

    public static void main(String[] args) {
        toPrimitiveData.unBoxing();
    }
}
